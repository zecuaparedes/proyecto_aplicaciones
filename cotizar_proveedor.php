<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
				<?php include'inc/incluye_bootstrap.php' ?>
			</head>
			<body>
				<?php include'inc/incluye_menu.php' ?>
				<div class="container">
            <div class="jumbotron">
                <h1>Cotizar con el proveedor</h1>
                <form role="form" id="login-form"
                      method="post" class="form-signin"
                      action="guardar_refaccion.php">
                    <br><br>
                    <div class="form-group">
                        <label for="id_refaccion">ID refaccion seleccionada (BUJIAS)</label>
                        <input type="text" class="form-control" id="id_refaccion" name="id_refaccion"
                               placeholder="Ingrega ID refaccion">
                             </div>
                             <div class="h2">
                        Selecciona el proveedor con el que estas cotizando (Requerido)
                    </div>
                    <br>
                    <label for="Refaccion">Nombre de la refaccion:</label>
            <select id="id_proveedor" name="refaccion">
            <option value="">seleccione al proveedor</option>
            <option value="proveedor">Auto-refacciones</option>
            <option value="proveedor">Autozone</option>
            <option value="proveedor">California</option>
        </select><br><br>
        <div class="form-group">
        <label>Fecha de solicitud de precio (requerido)</label><br><br>
        <input type="date" class="form-control" id="fecha_solicitud" name="fecha_solicitud" step="1" value="<?php echo date("y-m-d"); ?>" required>
                    </div>
                    
                    <div class="form-group">
                        <label>precio $ (requerido)</label>
                        <input type="number" class="form-control" id="precio" name="precio" step="0.01" placeholder="precio actual" style="text-transform:uppercase;" required>
                    </div>
                     <div class="form-group">
                        <label>Tel&eacute;fono </label>
                        <input type="tel" class="form-control" id="telefono" name="telefono"
                               placeholder="Ingresa tel&eacute;fono">
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>
        </body>
</html>
