<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
    </head>

    <body>
<?php include'inc/incluye_menu.php' ?>
<div class="container">
    <div class="jumbotron">

      <?php
      $sel = $con->prepare("SELECT *from marca");
      $sel->execute();
      $res = $sel->get_result();
      $row = mysqli_num_rows($res);
      ?>
      Elementos devueltos por la consulta: <?php echo $row ?>
      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
          <th>ID MARCA</th>
          <th>NOMBRE MARCA</th>
          </thead>
          <tfoot>
          <th>ID MARCA</th>
          <th>NOMBRE MARCA</th>
          </tfoot>
          <tbody>

<?php
$marcas_autos = array("FORD", "CHEVROLET", "TOYOTA", "HONDA", "NISSAN" , "BMW" , "DODGE" , "FERRARI" , "AUDI" , "DAEWOO" , "GMC" , "HYUNDAI" , "HUMMER" , "KIA" , "LEXUS" , "MERCEDES BENZ" , "MAZDA" , "RENAULT" , "SUZUKI" , "TESLA" , "VOLVO" , "VOLKSWAGEN VW" , "ROLLS-ROYCE" , "ISUZU" , "	LAMBORGHINI");
?>
<?php while ($f = $res->fetch_assoc()) { ?>
    <tr>
        <td><?php echo $f['marca_id'] ?></td>
        <td><?php echo $f['marca_nombre'] ?></td>
    </tr>
    <?php
}
$sel->close();
$con->close();
?>
<form>
  <label for="marca_auto">Selecciona una marca de auto:</label>
  <select name="marca_auto" id="marca_auto">
    <?php
    foreach ($marcas_autos as $marca) {
      echo "<option value='$marca'>$marca</option>";
    }
    ?>
  </select>
  <input type="submit" value="Enviar">
</form>
</div>
</div>
<?php
include 'inc/incluye_datatable_pie.php';
?>
</body>
</html>
