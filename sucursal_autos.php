<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
				<?php include'inc/incluye_bootstrap.php' ?>
			</head>
			<body>
				<?php include'inc/incluye_menu.php' ?>
				<div class="container">
            <div class="jumbotron">
                <h1>Registrar una sucursal</h1>
                <form role="form" id="login-form"
                      method="post" class="form-signin"
                      action="guardar_sucursal.php">

                    <div class="h2">
                        DATOS DEL PROVEEDOR Y SUCURSAL
                    </div>
                    <br><br>
                    <div class="form-group">
                        <label for="Modelo_del_auto">Modelo del Auto (requerido)</label>
                        <input type="text" class="form-control" id="Modelo_del_auto" name="Modelo_del_auto"
                               placeholder="Ingresa modelo de Auto">
                             </div>
                    <br>
      <label for="Sucursal">Nombre de proveedor:</label>
			<select name="sucursal">
			<option value="">seleccione un proveedor</option>
			<option value="proveedor">Auto-refacciones</option>
			<option value="proveedor">Autozone</option>
			<option value="proveedor">California</option>
		</select><br><br>

		<label for="Sucursal">Nombre de sucursal:</label>
		<select name="sucursal">
			<option value="">Seleccione una opción</option>
			<option value="sucursal Mexico">Sucursal Mexico</option>
			<option value="sucursal Puebla">Sucursal Puebla</option>
      <option value="sucursal Tlaxcala">Sucursal Tlaxcala</option>
		</select><br><br>


		<label for="marca">Modelo de Auto:</label>
		<select name="marca">
			<option value="">Seleccione una opción</option>
			<option value="Chevrolet">Chevrolet</option>
			<option value="Ford">Ford</option>
			<option value="Nissan">Nissan</option>
			<option value="Honda">Honda</option>
		</select><br><br>

                    <div class="form-group">
                        <label>Direcci&oacute;n</label>
                        <input type="text" class="form-control" id="direccion_de_sucursal" name="direccion_de_sucursal"
                               placeholder="Ingresa direcci&oacute;n (Tienda matriz)">
                    </div>

                    <div class="form-group">
                        <label>Tel&eacute;fono 1</label>
                        <input type="tel" class="form-control" id="telefono_1" name="telefono_1"
                               placeholder="Ingresa primer tel&eacute;fono">
                    </div>

                    <label>Tel&eacute;fono 2</label>
                    <input type="tel" class="form-control" id="telefono_2" name="telefono_2"
                           placeholder="Ingresa segundo tel&eacute;fono">
                    <br>
                    <div class="form-group">
                        <label for="correo_sucursal">Correo electr&oacute;nico</label>

                        <input type="email" class="form-control" id="correo_sucursal" name="correo_sucursal"
                               placeholder="Ingresa correo electr&oacute;nico">

                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>

    </body>
</html>
