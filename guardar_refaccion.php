<?php
include 'inc/conexion.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id_refaccion_post = strtoupper($_POST['id_refaccion']);
    $fecha_solicitud_post = strtoupper($_POST['fecha_solicitud']);
    $precio_post = strtoupper($_POST['precio']);
    $id_proveedor_post = strtoupper($_POST['id_proveedor']);
    $telefono_post = strtoupper($_POST['telefono']);
    $id_proveedor='';
    $ins=$con->prepare("INSERT INTO refacciones_proveedores VALUES(?,?,?,?,?,?)");
    $ins->
    bind_param("isssss",$id,$id_refaccion_post,$fecha_solicitud_post,$precio_post,$id_proveedor_post,$telefono_post);
    if($ins->execute()){
      header ("Location: alerta.php?tipo=exito&operacion=refaccion Guardada&destino=guardar_refaccion.php");
    }
    else{
        header ("Location: alerta.php?tipo=fracaso&operacion=refaccion No Guardada&destino=cotizar_proveedor.php");
    }
    $ins->close();
    $con->close();
	}
?>